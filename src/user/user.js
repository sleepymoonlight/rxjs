import _ from 'lodash';
import users from '../../utilities/users';

export const person = _.find(users, { person: 'User' });

export const user = () => {
    const root = document.getElementById('app-root');
    const block = document.createElement('div');
    block.innerHTML = `<p>This is ${person.person} page.</p>`;
    root.appendChild(block);
};
