import {fromEvent, of, Subject} from 'rxjs';
import {fromFetch} from 'rxjs/fetch';
import {switchMap, catchError, debounceTime, map} from 'rxjs/operators';

const button = document.getElementById('addTodo');
const input = document.getElementById('input');
const subject = new Subject();
let index = 1;


interface TodoDTO {
    userId: number;
    id: number;
    title: string;
    completed: string;
}

class TodoModel implements TodoDTO {
    completed: string = '';
    id: number = -1;
    title: string = '';
    userId: number = -1;
}

// tasks 1-4

subject.pipe(
    switchMap(() => {
        return fromFetch(`https://jsonplaceholder.typicode.com/todos/${index}`).pipe(
            switchMap(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    return of({error: true, message: `Error ${response.status}`});
                }
            }),
            catchError(err => {
                console.error(err);
                return of({error: true, message: err.message})
            })
        );
    })
).subscribe({
    next: result => {
        console.log(result);
        const todoModel: TodoModel = result;
        const root = document.createElement("div");
        root.innerHTML = todoModel.title;
        document.body.appendChild(root);
    },
    complete: () => console.log('done')
});

const onClick = () => {
    subject.next();
    index++;
};

// @ts-ignore
button.addEventListener('click', onClick);

// task 5

// @ts-ignore
const keyUp$ = fromEvent(input, 'change');

keyUp$
    .pipe(
        map((i: any) => i.currentTarget.value),
        debounceTime(1000)
    ).subscribe(console.log);
