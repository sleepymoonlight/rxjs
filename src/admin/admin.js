import _ from 'lodash';
import users from '../../utilities/users';

import './style.scss';

export const person = _.find(users, { person: 'Admin' });

export function admin() {
    const root = document.getElementById('app-root');
    const block = document.createElement('div');
    block.innerHTML = `<p>This is ${person.person} page.</p>`;
    root.appendChild(block);
}
