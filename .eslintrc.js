module.exports = {
    "parser": "babel-eslint",
    "rules": {
        "semi": [2,"always"],
        "curly": ["error"],
        "no-dupe-keys": 2,
        "func-names": [1, "always"]
    },
    "env": {
        "es6": true,
        "browser": true
    },
    "globals": {
        "__dirname" : true,
        "module" : true,
        "exports" : true,
        "process" : true,
        "require" : true
    },
    "extends": "eslint:recommended",
    "parserOptions" :{
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true,
            "experimentalObjectRestSpread": true,
            "modules": true
        }
    }
};
